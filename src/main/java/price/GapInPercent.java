package price;

import java.math.BigDecimal;

public class GapInPercent implements Gap {
    private final BigDecimal value;

    public GapInPercent(BigDecimal value) {
        assert value != null;
        this.value = value;
    }

    @Override
    public BigDecimal getValue() {
        return value;
    }
}
