package price;

import comparable.Comparable;
import currency.Currency;
import currency.CurrencyException;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Objects;

public class Price implements Gap, Comparable<Price> {
    private final BigDecimal value;
    private final Currency currency;

    public Price(BigDecimal value, Currency currency) {
        assert value != null;
        assert currency != null;

        this.value = value;
        this.currency = currency;
    }

    public BigDecimal getValue() {
        return value;
    }

    public Currency getCurrency() {
        return currency;
    }

    /**
     * Adds a given price to this. If the price to be added to this
     * has another currency a translation is done before the addition.
     * @param price The price to add to this
     * @return The new price
     */
    public Price add(Gap price) throws CurrencyException {
        assert price != null;
        if (price instanceof GapInPercent){
            BigDecimal factor = percentToFactor(price.getValue()).add(BigDecimal.ONE);
            return new Price(value.multiply(factor), currency);
        }
        if (price instanceof Price){
            if (!Objects.equals(((Price) price).getCurrency(), currency)){
                throw new CurrencyException("Currencies do not match. You have to do a currency translation.");
            }
            return new Price(value.add(price.getValue()), currency);
        }
        throw new RuntimeException("Unsupported price type given.");
    }

    /**
     * Subtracts a given price from this. If the price to be subtracted from this
     * has another currency a translation is done before the subtraction.
     * @param price The price to subtract from this
     * @return The new price
     */
    public Price subtract(Gap price) throws CurrencyException {
        assert price != null;
        if (price instanceof GapInPercent){
            BigDecimal factor = BigDecimal.ONE.subtract(percentToFactor(price.getValue()));
            return new Price(value.multiply(factor), currency);
        }
        if (price instanceof Price){
            if (!Objects.equals(((Price) price).getCurrency(), currency)){
                throw new CurrencyException("Currencies do not match. You have to do a currency translation.");
            }
            return new Price(value.subtract(price.getValue()), currency);
        }
        throw new RuntimeException("Unsupported price type given.");
    }

    /**
     * Multiplies this price with a given factor
     * @param factor The factor to multiply with
     * @return The multiplied price
     */
    public Price multiply(BigDecimal factor){
        assert factor != null;
        return new Price(value.multiply(factor), currency);
    }

    /**
     * Divides this price through a given divisor
     * @param divisor The value to divide this price through
     * @return The divided price
     */
    public Price divide(BigDecimal divisor){
        assert divisor != null;
        return new Price(value.divide(divisor, estimateDigitsForDivision(value, divisor), RoundingMode.HALF_UP), currency);
    }

    /**
     * Translates a given percent value to a factor.
     * For example if percent is 50 the result will be 0.5
     * @param percent The percent value to translate
     * @return The factor
     */
    private static BigDecimal percentToFactor(BigDecimal percent){
        BigDecimal divisor = BigDecimal.valueOf(100);
        return percent.divide(divisor, estimateDigitsForDivision(percent, divisor), RoundingMode.HALF_UP);
    }

    private static int estimateDigitsForDivision(BigDecimal divident, BigDecimal divisor){
        int integerCountsDivident = divident.precision()-divident.scale();
        int integerCountsDivisor = divisor.precision()-divisor.scale();
        int digitsDivident = divident.scale();

        int z = integerCountsDivisor-integerCountsDivident;
        if (z < 0){
            z = 0;
        }

        return z+digitsDivident;
    }

    @Override
    public boolean isSmallerThan(Price object) {
        if (!this.currency.equals(object.currency)){
            throw new RuntimeException("Price to be compared has different currency. Needs to be converted.");
        }
        return value.compareTo(object.getValue()) < 0;
    }

    @Override
    public boolean isEqual(Price object) {
        if (!this.currency.equals(object.currency)){
            throw new RuntimeException("Price to be compared has different currency. Needs to be converted.");
        }
        return value.compareTo(object.getValue()) == 0;
    }

    @Override
    public boolean isGreaterThan(Price object) {
        if (!this.currency.equals(object.currency)){
            throw new RuntimeException("Price to be compared has different currency. Needs to be converted.");
        }
        return value.compareTo(object.getValue()) > 0;
    }

    @Override
    public String toString() {
        return "Price{" +
                "value=" + value +
                ", currency=" + currency +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Price price = (Price) o;
        return value.compareTo(price.value) == 0 &&
                Objects.equals(currency, price.currency);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value, currency);
    }
}
