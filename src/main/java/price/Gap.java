package price;

import java.math.BigDecimal;

public interface Gap {
    BigDecimal getValue();
}
