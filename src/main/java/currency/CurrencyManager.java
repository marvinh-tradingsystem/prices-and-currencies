package currency;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import price.Price;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicReference;

public class CurrencyManager {
    private static class TranslationEntry{
        private final Currency from;
        private final Currency to;
        private AtomicReference<BigDecimal> translationFactor;

        private TranslationEntry(Currency from, Currency to, BigDecimal translationFactor) {
            this.from = from;
            this.to = to;
            this.translationFactor = new AtomicReference<>(translationFactor);
        }

        public Currency getFrom() {
            return from;
        }

        public Currency getTo() {
            return to;
        }

        public BigDecimal getTranslationFactor() {
            return translationFactor.get();
        }

        public void setTranslationFactor(BigDecimal factor){
            this.translationFactor.set(factor);
        }
    }

    private final Logger LOGGER = LoggerFactory.getLogger(CurrencyManager.class);

    private final Collection<TranslationEntry> translationEntries = ConcurrentHashMap.newKeySet();

    public CurrencyManager() {}

    public void set(Currency from, Currency to, BigDecimal translationFactor){
        assert from != null;
        assert to != null;
        assert translationFactor != null;
        Optional<TranslationEntry> entry = find(from, to);
        if (!entry.isPresent()){
            translationEntries.add(new TranslationEntry(from, to, translationFactor));
        } else {
            entry.get().setTranslationFactor(translationFactor);
        }
    }

    public Optional<BigDecimal> get(Currency from, Currency to){
        Optional<TranslationEntry> translationEntry = find(from, to);
        return translationEntry.map(TranslationEntry::getTranslationFactor);
    }

    public Price translate(Price price, Currency targetCurrency) throws CurrencyTranslationException {
        Optional<BigDecimal> factor = get(price.getCurrency(), targetCurrency);
        if (!factor.isPresent()){
            throw new CurrencyTranslationException(price.getCurrency(), targetCurrency, "Entry does not exist.");
        }
        return new Price(price.getValue().multiply(factor.get()), targetCurrency);
    }

    private Optional<TranslationEntry> find(Currency from, Currency to){
        return translationEntries.stream()
                .filter(translationEntry ->
                        translationEntry.getFrom().equals(from)
                        && translationEntry.getTo().equals(to))
                .findAny();
    }
}
