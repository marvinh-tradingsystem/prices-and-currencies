package currency;

public class CurrencyTranslationException extends Exception{
    private final Currency from;
    private final Currency to;
    private final String reason;

    public CurrencyTranslationException(Currency from, Currency to, String reason) {
        this.from = from;
        this.to = to;
        this.reason = reason;
    }

    @Override
    public String getMessage() {
        return String.format("Currency translation from %s to %s failed: %s", from.getName(), to.getName(), reason);
    }
}
