package currency;

import java.util.Objects;

public class Currency {
    public static class Symbol {
        private final String symbol;

        public Symbol(String symbol) {
            assert symbol != null;
            this.symbol = symbol;
        }

        public String get() {
            return symbol;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Symbol symbol1 = (Symbol) o;
            return symbol.equals(symbol1.symbol);
        }

        @Override
        public int hashCode() {
            return Objects.hash(symbol);
        }

        @Override
        public String toString() {
            return "Symbol{" + symbol + '}';
        }
    }

    private final String name;
    private final Symbol symbol;
    private final String abbreviation;

    public Currency(String name, Symbol symbol, String abbreviation) {
        assert name != null;
        assert symbol != null;
        assert abbreviation != null;
        this.name = name;
        this.symbol = symbol;
        this.abbreviation = abbreviation;
    }

    public Symbol getSymbol(){
        return symbol;
    }
    public String getName(){
        return name;
    }
    public String getAbbreviation() {
        return abbreviation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Currency currency = (Currency) o;
        return name.equals(currency.name) &&
                symbol.equals(currency.symbol) &&
                abbreviation.equals(currency.abbreviation);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, symbol, abbreviation);
    }

    @Override
    public String toString() {
        return "Currency{" +
                "name='" + name + '\'' +
                ", symbol=" + symbol +
                ", abbreviation='" + abbreviation + '\'' +
                '}';
    }
}
