package price;

import currency.Currency;
import currency.CurrencyException;
import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import java.math.BigDecimal;

class PriceTest {

    @Test
    void addSameCurrencyPrice() throws CurrencyException {
        Price a = new Price(BigDecimal.valueOf(5), new Currency("Euro", new Currency.Symbol("€"), "EUR"));
        Price b = new Price(BigDecimal.valueOf(2), new Currency("Euro", new Currency.Symbol("€"), "EUR"));
        Price c = a.add(b);
        assertEquals(0, c.getValue().compareTo(BigDecimal.valueOf(7)));
        assertEquals(new Currency("Euro", new Currency.Symbol("€"), "EUR"), c.getCurrency());
    }

    @Test
    void addSameCurrencyPercent() throws CurrencyException {
        Price a = new Price(BigDecimal.valueOf(5), new Currency("Euro", new Currency.Symbol("€"), "EUR"));
        GapInPercent b = new GapInPercent(BigDecimal.valueOf(50));
        Price c = a.add(b);
        assertEquals(0, c.getValue().compareTo(BigDecimal.valueOf(7.5)));
        assertEquals(new Currency("Euro", new Currency.Symbol("€"), "EUR"), c.getCurrency());
    }

    @Test
    void addDifferentCurrencyPrice() throws CurrencyException {
        Price a = new Price(BigDecimal.valueOf(5), new Currency("Euro", new Currency.Symbol("€"), "EUR"));
        Price b = new Price(BigDecimal.valueOf(2), new Currency("US-Dollar", new Currency.Symbol("$"), "USD"));
        assertThrows(CurrencyException.class, () -> {
            a.add(b);
        });
    }

    @Test
    void subtractSameCurrencyPrice() throws CurrencyException {
        Price a = new Price(BigDecimal.valueOf(5), new Currency("Euro", new Currency.Symbol("€"), "EUR"));
        Price b = new Price(BigDecimal.valueOf(2), new Currency("Euro", new Currency.Symbol("€"), "EUR"));
        Price c = a.subtract(b);
        assertEquals(0, c.getValue().compareTo(BigDecimal.valueOf(3)));
        assertEquals(new Currency("Euro", new Currency.Symbol("€"), "EUR"), c.getCurrency());
    }

    @Test
    void subtractSameCurrencyPercent() throws CurrencyException {
        Price a = new Price(BigDecimal.valueOf(5), new Currency("Euro", new Currency.Symbol("€"), "EUR"));
        GapInPercent b = new GapInPercent(BigDecimal.valueOf(50));
        Price c = a.subtract(b);
        assertEquals(0, c.getValue().compareTo(BigDecimal.valueOf(2.5)));
        assertEquals(new Currency("Euro", new Currency.Symbol("€"), "EUR"), c.getCurrency());
    }

    @Test
    void subtractDifferentCurrencyPrice() throws CurrencyException {
        Price a = new Price(BigDecimal.valueOf(5), new Currency("Euro", new Currency.Symbol("€"), "EUR"));
        Price b = new Price(BigDecimal.valueOf(2), new Currency("US-Dollar", new Currency.Symbol("$"), "USD"));
        assertThrows(CurrencyException.class, () -> {
            a.add(b);
        });
    }

    @Test
    void multiply() {
        Price a = new Price(BigDecimal.valueOf(5), new Currency("Euro", new Currency.Symbol("€"), "EUR"));
        Price c = a.multiply(BigDecimal.valueOf(2));
        assertEquals(0, c.getValue().compareTo(BigDecimal.valueOf(10)));
        assertEquals(new Currency("Euro", new Currency.Symbol("€"), "EUR"), c.getCurrency());
    }

    @Test
    void divide() {
        Price a = new Price(BigDecimal.valueOf(5.0), new Currency("Euro", new Currency.Symbol("€"), "EUR"));
        Price c = a.divide(BigDecimal.valueOf(2));
        assertEquals(0, c.getValue().compareTo(BigDecimal.valueOf(2.5)));
        assertEquals(new Currency("Euro", new Currency.Symbol("€"), "EUR"), c.getCurrency());
    }

    @Test
    void isSmallerThan() {
        Price a = new Price(BigDecimal.valueOf(5), new Currency("Euro", new Currency.Symbol("€"), "EUR"));
        Price b = new Price(BigDecimal.valueOf(2), new Currency("Euro", new Currency.Symbol("€"), "EUR"));
        assertTrue(b.isSmallerThan(a));
        assertFalse(a.isSmallerThan(b));
    }

    @Test
    void isEqual() {
        Price a = new Price(BigDecimal.valueOf(5), new Currency("Euro", new Currency.Symbol("€"), "EUR"));
        Price b = new Price(BigDecimal.valueOf(2), new Currency("Euro", new Currency.Symbol("€"), "EUR"));
        Price c = new Price(BigDecimal.valueOf(5), new Currency("Euro", new Currency.Symbol("€"), "EUR"));
        assertTrue(a.isEqual(c));
        assertFalse(a.isEqual(b));
        assertFalse(b.isEqual(c));
    }

    @Test
    void isGreaterThan() {
        Price a = new Price(BigDecimal.valueOf(5), new Currency("Euro", new Currency.Symbol("€"), "EUR"));
        Price b = new Price(BigDecimal.valueOf(2), new Currency("Euro", new Currency.Symbol("€"), "EUR"));
        assertTrue(a.isGreaterThan(b));
        assertFalse(b.isGreaterThan(a));
    }
}