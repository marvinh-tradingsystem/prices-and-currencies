package currency;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import price.Price;

import java.math.BigDecimal;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class CurrencyManagerTest {

    @Test
    void getEntryExists() {
        CurrencyManager currencyManager = new CurrencyManager();
        currencyManager.set(
                new Currency("Euro", new Currency.Symbol("€"), "EUR"),
                new Currency("US-Dollar", new Currency.Symbol("$"), "USD"),
                BigDecimal.valueOf(1.5)
        );
        Optional<BigDecimal> factor = currencyManager.get(
                new Currency("Euro", new Currency.Symbol("€"), "EUR"),
                new Currency("US-Dollar", new Currency.Symbol("$"), "USD")
        );
        assertTrue(factor.isPresent());
        assertEquals(0, factor.get().compareTo(BigDecimal.valueOf(1.5)));
    }

    @Test
    void getEntryNotExists() {
        CurrencyManager currencyManager = new CurrencyManager();
        currencyManager.set(
                new Currency("Euro", new Currency.Symbol("€"), "EUR"),
                new Currency("US-Dollar", new Currency.Symbol("$"), "USD"),
                BigDecimal.valueOf(1.5)
        );
        Optional<BigDecimal> factor = currencyManager.get(
                new Currency("US-Dollar", new Currency.Symbol("$"), "USD"),
                new Currency("Euro", new Currency.Symbol("€"), "EUR")
        );
        assertFalse(factor.isPresent());
    }

    @Test
    void translateEntryExists() throws CurrencyTranslationException {
        CurrencyManager currencyManager = new CurrencyManager();
        currencyManager.set(
                new Currency("Euro", new Currency.Symbol("€"), "EUR"),
                new Currency("US-Dollar", new Currency.Symbol("$"), "USD"),
                BigDecimal.valueOf(1.5)
        );
        Price translatedPrice = currencyManager.translate(
                new Price(BigDecimal.valueOf(5), new Currency("Euro", new Currency.Symbol("€"), "EUR")),
                new Currency("US-Dollar", new Currency.Symbol("$"), "USD")
        );
        assertEquals(0, translatedPrice.getValue().compareTo(BigDecimal.valueOf(7.5)));
        assertEquals(new Currency("US-Dollar", new Currency.Symbol("$"), "USD"), translatedPrice.getCurrency());
    }

    @Test
    void translateEntryNotExists() throws CurrencyTranslationException {
        CurrencyManager currencyManager = new CurrencyManager();
        currencyManager.set(
                new Currency("Euro", new Currency.Symbol("€"), "EUR"),
                new Currency("US-Dollar", new Currency.Symbol("$"), "USD"),
                BigDecimal.valueOf(1.5)
        );
        assertThrows(CurrencyTranslationException.class, () -> currencyManager.translate(
                new Price(BigDecimal.valueOf(5), new Currency("US-Dollar", new Currency.Symbol("$"), "USD")),
                new Currency("Euro", new Currency.Symbol("€"), "EUR")
        ));
    }
}